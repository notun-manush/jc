import React from "react";
import Check from "./icons/Check";

interface Props {}

const Product = (props: Props) => {
  const desc =
    "Lorem ipsum dolor sit amet consectetur adipisicing elit In oditexercitationem fuga id nam quia";
  return (
    <div>
      <div className="py-6">
        <div className="flex min-h-[20rem]  mx-auto overflow-hidden bg-white rounded-lg shadow-lg dark:bg-gray-800 max-w-1/2">
          <div
            className="w-1/3 bg-cover"
            style={{
              backgroundImage:
                'url("https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fstatic1.squarespace.com%2Fstatic%2F569f9307b204d5300732ef63%2F56a4eb3ebfe873f61a2f19fd%2F56a4f5811a5203313c34390e%2F1453651346178%2Fcover-440x610.jpg&f=1&nofb=1")',
            }}
          ></div>
          <div className="flex flex-col justify-between w-2/3 p-4 ">
            <div className="info">
              <h1 className="text-2xl font-bold text-gray-900 dark:text-gray-100">
                Productive Muslim
              </h1>
              <p className="mt-2 text-sm text-gray-600 dark:text-gray-200">
                {desc}
              </p>
            </div>
            <div className="flex items-center justify-between mt-3">
              <p className="flex items-center gap-1 py-0.5 px-1 rounded-sm text-xs font-semibold text-gray-700 uppercase bg-green-500/70 dark:text-green-300">
                <Check className="text-md" /> lowest price
              </p>
              <button className="px-3 py-2 text-xs font-bold text-white uppercase bg-pink-600 rounded-sm rounded">
                Order Books
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Product;
