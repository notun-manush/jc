import Swup from "swup";
import SwupA11yPlugin from "@swup/a11y-plugin";
import SwupSlideTheme from "@swup/slide-theme";
import SwupHeadPlugin from "@swup/head-plugin";

const options = {
  plugins: [new SwupA11yPlugin(), new SwupHeadPlugin(), new SwupSlideTheme()],
};

const swup = new Swup(options);
