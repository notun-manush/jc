import React, { useState } from "react";

interface Props {}

const data = {
  properties: [
    {
      _id: "593e9297e17df20c4a237d42",
      index: 0,
      price: 937180,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property01.jpg",
      city: "Singer",
      address: "914 Argyle Road",
      latitude: -33.944576,
      longitude: 151.25584,
      bedrooms: 2,
      bathrooms: 2,
      carSpaces: 2,
    },
    {
      _id: "593e9297ec4cca9c56bf61af",
      index: 1,
      price: 703725,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property02.jpg",
      city: "Machias",
      address: "255 Raleigh Place",
      latitude: -33.944471,
      longitude: 151.2541,
      bedrooms: 2,
      bathrooms: 1,
      carSpaces: 0,
    },
    {
      _id: "593e929773c71925e5d7c11c",
      index: 2,
      price: 837111,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property03.jpg",
      city: "Bend",
      address: "580 Amber Street",
      latitude: -33.944644,
      longitude: 151.24834,
      bedrooms: 3,
      bathrooms: 2,
      carSpaces: 0,
    },
    {
      _id: "593e92973d4d15eedb129aee",
      index: 3,
      price: 648223,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property04.jpg",
      city: "Ivanhoe",
      address: "854 Varanda Place",
      latitude: -33.940396,
      longitude: 151.2469,
      bedrooms: 3,
      bathrooms: 2,
      carSpaces: 0,
    },
    {
      _id: "593e929728b7f8543b7907e1",
      index: 4,
      price: 771826,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property05.jpg",
      city: "Lydia",
      address: "857 Rockaway Parkway",
      latitude: -33.944562,
      longitude: 151.2503,
      bedrooms: 2,
      bathrooms: 1,
      carSpaces: 1,
    },
    {
      _id: "593e92975996a6263d146444",
      index: 5,
      price: 686238,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property01.jpg",
      city: "Rosedale",
      address: "543 Harman Street",
      latitude: -33.939304,
      longitude: 151.25046,
      bedrooms: 2,
      bathrooms: 1,
      carSpaces: 1,
    },
    {
      _id: "593e9297a13e246da5ba0884",
      index: 6,
      price: 595377,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property02.jpg",
      city: "Allison",
      address: "787 Gerald Court",
      latitude: -33.937819,
      longitude: 151.25774,
      bedrooms: 2,
      bathrooms: 2,
      carSpaces: 2,
    },
    {
      _id: "593e9297afa51a013fcd07d5",
      index: 7,
      price: 696189,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property03.jpg",
      city: "Nicut",
      address: "416 Vandervoort Avenue",
      latitude: -33.943154,
      longitude: 151.24687,
      bedrooms: 4,
      bathrooms: 1,
      carSpaces: 1,
    },
    {
      _id: "593e92972bd8fd05f2ab1a89",
      index: 8,
      price: 716114,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property04.jpg",
      city: "Canterwood",
      address: "437 Mill Street",
      latitude: -33.943723,
      longitude: 151.25278,
      bedrooms: 3,
      bathrooms: 1,
      carSpaces: 0,
    },
    {
      _id: "593e9297fe2e6d32c859ecf4",
      index: 9,
      price: 739852,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property05.jpg",
      city: "Cobbtown",
      address: "553 Friel Place",
      latitude: -33.942459,
      longitude: 151.24941,
      bedrooms: 2,
      bathrooms: 1,
      carSpaces: 1,
    },
    {
      _id: "593e92970481ccf52f948e6d",
      index: 10,
      price: 625917,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property01.jpg",
      city: "Blue",
      address: "687 Opal Court",
      latitude: -33.939606,
      longitude: 151.25289,
      bedrooms: 4,
      bathrooms: 1,
      carSpaces: 2,
    },
    {
      _id: "593e9297e09417746b79e1c5",
      index: 11,
      price: 591236,
      picture:
        "https://ihatetomatoes.net/demos/_rw/01-real-estate/tn_property02.jpg",
      city: "Craig",
      address: "183 Canton Court",
      latitude: -33.938557,
      longitude: 151.24773,
      bedrooms: 2,
      bathrooms: 1,
      carSpaces: 0,
    },
  ],
};

const Slider = (props: Props) => {
  const [properties, setProperties] = useState(data.properties);
  const [property, setProperty] = useState(data.properties[5]);

  const nextProperty = () => {
    const newIndex =
      property.index === 11 ? property.index - 10 : property.index + 1;
    setProperty(data.properties[newIndex]);
  };

  const prevProperty = () => {
    const newIndex =
      property?.index === 1 ? property.index + 10 : property?.index - 1;
    setProperty(data.properties[newIndex]);
  };

  return (
    <div className="flex flex-col justify-start min-h-screen overflow-x-hidden bg-blue-500">
      <button
        onClick={() => prevProperty()}
        className="px-4 py-1 m-2 bg-blue-200 hover:bg-blue-300"
      >
        Prev
      </button>
      <button
        onClick={() => nextProperty()}
        className="px-4 py-1 m-2 bg-blue-200 hover:bg-blue-300"
      >
        Next
      </button>
      <div className="relative  bg-red-600 max-w-[226px] mx-auto">
        <div
          style={{
            transform: `translateX(-${
              property.index * (100 / properties.length)
            }%)`,
          }}
          className={`absolute transition-all duration-300 top-0 transform left-0 flex gap-4  wrapper`}
        >
          {properties.map((property) => (
            <div key={property._id}>
              <Card property={property} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Slider;

const Card = ({ property }) => {
  return (
    <div className="z-20 min-w-[200px]  flex items-center justify-center flex-1 ">
      <div className="container flex justify-center">
        <div className="max-w-[15rem]  ">
          <div className="relative px-8 md:h-[300px] py-4 bg-white rounded-sm shadow-lg dark:bg-gray-800 hover:shadow-xl">
            {/* icon-container */}
            <div className="min-h-[4rem] flex text-pink-600 justify-center items-center">
              <div className="relative flex items-center justify-center w-20 h-20 text-4xl transition duration-500 bg-gray-200 rounded-full dark:bg-gray-700 hover:after:bg-yellow-600 icon">
                icon{property.index}
                <div className="absolute transition duration-500 inset-1 hexagon o hover:animate-spin " />
              </div>
            </div>
            <div className="flex flex-col items-center p-2 text-center rounded-lg dark:text-gray-100">
              <h1 className="mb-3 text-xl font-bold text-gray-700 dark:text-gray-100 hover:text-gray-900 hover:cursor-pointer ">
                {property.city}
              </h1>
              <p className="text-sm tracking-wide text-gray-700 dark:text-gray-200">
                {property.address}
              </p>
              <button className="px-6 py-1.5 mt-4 text-sm flex items-center gap-1 font-semibold transition duration-300 bg-pink-600 rounded-sm shadow-md hover:shadow-lg">
                Enter
                <enter className="text-xl" />
              </button>
            </div>
            {/* buy */}
          </div>
        </div>
      </div>
    </div>
  );
};
